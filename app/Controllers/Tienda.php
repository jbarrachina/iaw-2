<?php namespace App\Controllers;

use App\Models\ArticlesModel;
use App\Models\DisenadoresModel;
use Config\Services;

class Tienda extends BaseController {

    protected $sesion;
    protected $datos;
    protected $datosDis;
    protected $auth;

    /**
     * Constructor.
     */
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->datos = new ArticlesModel();
        $this->datosDis = new DisenadoresModel();
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();
    }
    
    public function index()
        {
            //solicitamos los datos al modelo
        //definimos como queremos paginar
        $data = [
            'titulo' => "Listado de Artículos Paginados",
            'articulos' => $this->datos->paginate(8),
            'pager' => $this->datos->pager
        ];
        //pasamos los datos a la vista
        echo view('articulos/mostrar', $data);
        }

    public function nuevo(){
        if (!$this->auth->loggedIn())
        {
            return redirect()->to(site_url('auth/login'));
        }
        helper('form');
        $data ['titulo'] = "Nuevo artículo";
        //regla para validar la imagen.
        $image_validate = [
            'imagen' => 'uploaded[imagen]|ext_in[imagen,jpg]|max_size[imagen,100]|max_dims[imagen,420,520]',
            'descripcion' => 'is_unique[articulos.descripcion]',
        ];
        if ($this->request->getVar('boton') != NULL && $this->validate(array_merge($this->datos->getValidationRules(),$image_validate))){
            $articulo = $this->request->getPost();
            $this->datos->insert($articulo);
            //subimos y le asignamos un nombre al fichero
            try {
                $path = $this->request->getFile('imagen')->store('camisetas/', str_replace(" ", "_", $articulo['descripcion'].".jpg"));
                $this->session->setFlashdata('mensaje_exito',"Registro insertado: {$articulo['descripcion']} ");
                } catch (Exception $e) {
                $this->session->setFlashdata('mensaje_exito',"Error al subir la imagen {$articulo['descripcion']}:  ".$e->getMessage());
                }  
            return redirect()->to(site_url('tienda/nuevo'));
        } else {
            $disenadoresModel = new DisenadoresModel();
            $data['options'] = $disenadoresModel->getDisenadores();
            echo view('articulos/form_alta', $data);
        }
    }

    public function edita($id){
        helper('form');
        //recuperamos el artículo
        $data['articulo'] = $this->datos->find($id);
        $data ['titulo'] = "Edita un artículo - {$data['articulo']->id} -";
        if ($this->request->getVar('boton') != NULL && $this->validate($this->datos->getValidationRules())){
            $articulo = $this->request->getPost();
            $this->datos->update($id, $articulo);
            $data['mensaje'] = 'Cambio realizado con éxito';
        }
        echo view('articulos/form_edita', $data);
    }

    public function borrar($id = 0)
    {
        if ($id !== 0) 
            {
                $this->datos->delete($id);
                return redirect()->to(site_url('tienda'));
            }
        else 
            {
                echo "Debes especificar un artículo";
            }
    }
   
    public function comprar ($id){
        //Comprobar si el artículo está en el carro
        if ($this->session->has('carro')) {
            //existe el carro
            $carro = $this->session->get('carro');
            if (!isset($carro[$id])){
                //no existe el elmento
                $articulo = $this->datos->find($id);
                $articulo->cantidad = 1;
                $carro[$id]=$articulo; //añadimos un elemento al array
            } else {
                //sólo incrementar la cantidad comprada   
                $carro[$id]->cantidad++;
            }
        } else {
            //crear el carro
            $articulo = $this->datos->find($id);
            $carro[$id] = $articulo;
            $articulo->cantidad = 1;
        }
        //var_dump($this->session->carro);
        //y en todos los casos guardar la variable de sesion
        $this->session->set('carro',$carro);
        return redirect()->to(site_url('/'));
    }

    public function carro(){
        //definimos como queremos paginar
        $data = [
            'titulo' => "Carro de la compra",
        ];
        //Ahora los datos no hemos de buscarlos en la base de datos, 
        // están en la variable de sesión.
        echo view('articulos/carro', $data);
    }

    public function borraCarro(){
        $this->session->remove('carro');
        return redirect()->to(site_url('tienda/carro'));
    } 

    public function seed(){
        $articulos = $this->datosDis->findAll();
        echo '<pre>';
        print_r($articulos);
        echo '</pres>';
    }
    
}